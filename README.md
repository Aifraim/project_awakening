A feladat egy 3. nézetű fantasy szerepjáték elkészítése Unreal Engine 4 játékmotor Blueprint technikájának alkalmazásával.

A project betöltéséhez az Awekening.uproject nevű file-t kell megnyitni az Epic Games Launcher segítségével.

A projectet később lehet app formájában is elindítani, ehhez fontos funkciók kellenek még hozzá, hogy tesztelni lehessen éles környezetben.